﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabajador
{
   public  interface Pago: IFormattable
    {
        double PagoSalario(String tipoJornada, int horasTrabajadas);
        void mostrarPago();
    }
}
