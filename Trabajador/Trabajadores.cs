﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabajador
{
    public  class Trabajadores
    {
        public Trabajadores()
        {

        }

        public Trabajadores(int idPersona, string nombre, string apellido, int cedula, int edad, int horasTrabajadas, int tipoJornada)
        {
            this.idPersona = idPersona;
            this.nombre = nombre;
            this.apellido = apellido;
            this.cedula = cedula;
            this.edad = edad;
            this.horasTrabajadas = horasTrabajadas;
            this.tipoJornada = tipoJornada;
        }
        private int idPersona;
        private string nombre;
        private string apellido;
        private int cedula;
        private int edad;
        private int horasTrabajadas;
        private int tipoJornada;

        public int IdPersona { get => idPersona; set => idPersona = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido{ get => apellido; set => apellido = value; }
        public int Cedula { get => cedula; set => cedula = value; }
        public int Edad { get => edad; set => edad = value; }
        public int HorasTrabajadas { get => horasTrabajadas; set => horasTrabajadas = value; }
        public int TipoJornada { get => tipoJornada; set => tipoJornada = value; }
        
    }
   
}
