﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabajador
{
    public class Duenno
    {


        public Duenno()
        {
        }

        public Duenno(int idPersona, string nombre, string apellido, int cedula, int edad, string cargosAdministrativos, int acciones)
        {
            this.idPersona = idPersona;
            this.nombre = nombre;
            this.apellido = apellido;
            this.cedula = cedula;
            this.edad = edad;
            this.cargosAdministrativos = cargosAdministrativos;
            this.acciones = acciones;
        }

        private int idPersona { get; set; }
        private string nombre { get; set; }
        private string apellido { get; set; }
        private int cedula { get; set; }
        private int edad { get; set; }
        private string cargosAdministrativos { get; set; }
        private int acciones { get; set; }


        public int IdPersona { get => idPersona; set => idPersona = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Cedula { get => cedula; set => cedula = value; }
        public int Edad { get => edad; set => edad = value; }
        public string CargosAdministrativos { get => cargosAdministrativos; set => cargosAdministrativos = value; }
        public int Acciones { get => acciones; set => acciones = value; }

    }
}

