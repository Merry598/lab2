﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabajador
{
    public abstract class  AbsPersona
    {

        public abstract int idPersona { get; set; }
        public abstract string nombre { get; set; }
        public abstract string apellido { get; set; }
        public abstract int cedula { get; set; }
        public abstract int edad { get; set; }
        

    }
}
